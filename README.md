# itk-demo-sw Meta Repo Tools

## Introduction

The itk-demo-sw repo is a meta-[meta repo](https://patrickleet.medium.com/mono-repo-or-multi-repo-why-choose-one-when-you-can-have-both-e9c77bd0c668)
to work with the [itk-demo-sw](https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw)
GitLab subgroup itself.

DeMi is composed of several repos for the micro and secondary services,
most of which are monorepos themselves.
It's GitLab group can therefore be seen as a meta-repo.
Since it is however not an acutal Git repo itself, we need tooling to efficiently
work with this mixed eco-system.

The proposed workflow is:

- Check out this repo as the root of your DeMi development working tree.

```
git clone https://gitlab.cern.ch/atlas-itk-pixel-systemtest/itk-demo-sw/itk-demo-sw.git
```

- Use (some of) the proposed tools, like `glab`, to clone all desired repos.
- Add the checked out repos to `.gitignore` in order not to add them to *this* repo (For this repo only contribute back *tooling*.)

So, nothing changes compared to checking out every repo indiviudally, apart from the reduced
effort.
No weird side-effects and increased maintenance like from Git submodules or similar.

## Prerequisites

### Tool Dependencies

- As usual you can use the `./bootstrap` script to check which tooling is available.
```
./bootstrap
```
- Tooling is predominantly Docker and docker compose. We try to use containerized versions of tools so you do not have to install locally. To run natively many tools need Python >= 3.9. see [python in the itk-demo-sw docs](https://demi.docs.cern.ch/general/python/).

### Setup

- Use the following environment variables. Add them your `.bashrc` (and put a personal GitLab token in `~/.gitlab-token`).
```shell
export GITLAB_TOKEN=$(cat ~/.gitlab-token)
export GITLAB_URL=https://gitlab.cern.ch/
export GITLAB_HOST=https://gitlab.cern.ch
```
- These can be soruced using setup script `

```
. ./setup
```

## Suggested tools

### glab

To work with CERN GitLab we recommend the official GitLab CLI [glab](https://gitlab.com/gitlab-org/cli):

- First you need to create a GitLab personal access token with full api access and store it in `~/.gitlab-token`.
- Source the `setup` script. This
  - creates an alias to run the Docker version of `glab`.
  - configures `glab` to use CERN Gitlab and HTTPS protocol (ssh not installed in official glab Docker image).
- authenticate to CERN GitLab and check everything went fine

- n.b.: If your "user working directory" (eg. on some local scratch space) deviates from your actual user home directory (eg. your AFS home or some NFS mounted home which is not supported by Docker), you need to specifically export `$HOME`!

- clone all public, non-archived repos in the `itk-demo-sw` subgroup:

```shell
glab repo clone -g atlas-itk-pixel-systemtest/itk-demo-sw -p --paginate -a=false -v="public"
```

(make sure to configure the Git credential cache correctly since this clones *all* repos)


- [gitlab](https://python-gitlab.readthedocs.io/en/stable/) python-gitlab CLI

## Not suggested tools

- [gitlabber](https://github.com/ezbz/gitlabber) doesn't seem to work out of the box - unmaintained?

## References

- [stackoverflow "How to clone all projects of a group at once in gitlab"](https://stackoverflow.com/questions/29099456/how-to-clone-all-projects-of-a-group-at-once-in-gitlab)
- [meta Tool](https://github.com/mateodelnorte/meta)
- [gita](https://github.com/nosarthur/gita)
- [myrepos](https://myrepos.branchable.com/)
